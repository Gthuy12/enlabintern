﻿using System;

namespace Calculator_OOP
{
    class Program
    {
        public delegate double CalculatorDelegate(double x, double y);
        static void Main(string[] args)
        {
            bool checkMenu = true;
            while (checkMenu)
            {
                checkMenu = MainMenu();
            }
        }
        static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1/Addition");
            Console.WriteLine("2/Subtraction");
            Console.WriteLine("3/Multiplication");
            Console.WriteLine("4/Division");
            Console.WriteLine("5/Quit");
            switch (Console.ReadLine())
            {
                case "1":
                    Cal(Add);
                    return true;
                case "2":
                    Cal(Sub);
                    return true;
                case "3":
                    Cal(Mul);
                    return true;
                case "4":
                    Cal(Div);
                    return true;
                case "5":
                    return false; 
                default:
                    return true;
            }
        }
        public static void Cal(CalculatorDelegate func)
        {
            // 1/ Enter 2 params
            // 2/ Using delegate call calculate functions
            // 3/ Write result in console
            Console.WriteLine("Enter 1st input");
            double a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter 2nd input");
            double b = Convert.ToInt32(Console.ReadLine());
            double result = func(a, b);
            Console.WriteLine("The result is {0}", result);
            Console.Write("\r\nPress Enter to return to Main Menu");
            Console.ReadLine();
        }
        public static double Add(double a, double b)
        {
            double result = a + b;
            return result;
        }
        public static double Sub(double a, double b)
        {
            double result = a - b;
            return result;
        }
        public static double Mul(double a, double b)
        {
            double result = a * b;
            return result;
        }
        public static double Div(double a, double b)
        {
            double result = a / b;
            return result;
        }
    }
}
