﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoApp_v1
{
    public class Task
    {
        private string _Title;
        private string _Description;
        private DateTime _CreatedAt;
        private DateTime _UpdatedAt;
        private DateTime _FinishedAt;
        private int _Type;
        public string Title { get => _Title; set => _Title = value; }
        public string Description { get => _Description; set => _Description = value; }
        public DateTime CreatedAt { get => _CreatedAt; set => _CreatedAt = value; }
        public DateTime UpdatedAt { get => _UpdatedAt; set => _UpdatedAt = value; }
        public DateTime FinishedAt { get => _FinishedAt; set => _FinishedAt = value; }
        public int Type { get => _Type; set => _Type = value; }

        public Task(string title, string description, DateTime create, DateTime update, DateTime finish, int type) 
        {
            this.Title = title;
            this.Description = description;
            this.CreatedAt = create;
            this.UpdatedAt = update;
            this.FinishedAt = finish;
            this.Type = type;
        }
    }
}
