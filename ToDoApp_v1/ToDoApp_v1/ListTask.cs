﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoApp_v1
{
    public class ListTask
    {
        private string _Name;
        private List<Task> _Tasks;

        public ListTask(string name)
        {
            this.Name = name;
            this.Tasks = new List<Task>();
        }

        public string Name { get => _Name; set => _Name = value; }
        public List<Task> Tasks { get => _Tasks; set => _Tasks = value; }
    }
}
