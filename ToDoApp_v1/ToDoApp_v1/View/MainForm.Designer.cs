﻿
namespace ToDoApp_v1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbBacklog = new System.Windows.Forms.ListBox();
            this.lbResolved = new System.Windows.Forms.ListBox();
            this.lbClosed = new System.Windows.Forms.ListBox();
            this.btnAddTask = new System.Windows.Forms.Button();
            this.lbTrash = new System.Windows.Forms.ListBox();
            this.lblBacklog = new System.Windows.Forms.Label();
            this.lblResolved = new System.Windows.Forms.Label();
            this.lblClosed = new System.Windows.Forms.Label();
            this.lblTrash = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbBacklog
            // 
            this.lbBacklog.AllowDrop = true;
            this.lbBacklog.FormattingEnabled = true;
            this.lbBacklog.Location = new System.Drawing.Point(26, 41);
            this.lbBacklog.Name = "lbBacklog";
            this.lbBacklog.Size = new System.Drawing.Size(158, 342);
            this.lbBacklog.TabIndex = 0;
            this.lbBacklog.DisplayMember = "title";
            this.lbBacklog.DragDrop += new System.Windows.Forms.DragEventHandler(this.lb_DragDrop);
            this.lbBacklog.DragOver += new System.Windows.Forms.DragEventHandler(this.lb_DragOver);
            this.lbBacklog.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_MouseDown);
            // 
            // lbResolved
            // 
            this.lbResolved.AllowDrop = true;
            this.lbResolved.FormattingEnabled = true;
            this.lbResolved.Location = new System.Drawing.Point(210, 41);
            this.lbResolved.Name = "lbResolved";
            this.lbResolved.Size = new System.Drawing.Size(152, 342);
            this.lbResolved.TabIndex = 1;
            this.lbResolved.DisplayMember = "title";
            this.lbResolved.DragDrop += new System.Windows.Forms.DragEventHandler(this.lb_DragDrop);
            this.lbResolved.DragOver += new System.Windows.Forms.DragEventHandler(this.lb_DragOver);
            this.lbResolved.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_MouseDown);
            // 
            // lbClosed
            // 
            this.lbClosed.AllowDrop = true;
            this.lbClosed.FormattingEnabled = true;
            this.lbClosed.Location = new System.Drawing.Point(389, 41);
            this.lbClosed.Name = "lbClosed";
            this.lbClosed.Size = new System.Drawing.Size(154, 342);
            this.lbClosed.TabIndex = 2;
            this.lbClosed.DisplayMember = "title";
            this.lbClosed.DragDrop += new System.Windows.Forms.DragEventHandler(this.lb_DragDrop);
            this.lbClosed.DragOver += new System.Windows.Forms.DragEventHandler(this.lb_DragOver);
            this.lbClosed.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_MouseDown);
            // 
            // btnAddTask
            // 
            this.btnAddTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTask.Location = new System.Drawing.Point(434, 389);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.Size = new System.Drawing.Size(109, 33);
            this.btnAddTask.TabIndex = 3;
            this.btnAddTask.Text = "New Task";
            this.btnAddTask.UseVisualStyleBackColor = true;
            this.btnAddTask.Click += new System.EventHandler(this.btnAddTask_Click);
            // 
            // lbTrash
            // 
            this.lbTrash.AllowDrop = true;
            this.lbTrash.FormattingEnabled = true;
            this.lbTrash.Location = new System.Drawing.Point(150, 396);
            this.lbTrash.Name = "lbTrash";
            this.lbTrash.Size = new System.Drawing.Size(34, 43);
            this.lbTrash.TabIndex = 4;
            this.lbTrash.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbTrash_DragDrop);
            this.lbTrash.DragOver += new System.Windows.Forms.DragEventHandler(this.lb_DragOver);
            // 
            // lblBacklog
            // 
            this.lblBacklog.AutoSize = true;
            this.lblBacklog.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBacklog.Location = new System.Drawing.Point(21, 13);
            this.lblBacklog.Name = "lblBacklog";
            this.lblBacklog.Size = new System.Drawing.Size(89, 25);
            this.lblBacklog.TabIndex = 5;
            this.lblBacklog.Text = "Backlog";
            // 
            // lblResolved
            // 
            this.lblResolved.AutoSize = true;
            this.lblResolved.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResolved.Location = new System.Drawing.Point(206, 14);
            this.lblResolved.Name = "lblResolved";
            this.lblResolved.Size = new System.Drawing.Size(102, 25);
            this.lblResolved.TabIndex = 6;
            this.lblResolved.Text = "Resolved";
            // 
            // lblClosed
            // 
            this.lblClosed.AutoSize = true;
            this.lblClosed.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClosed.Location = new System.Drawing.Point(385, 14);
            this.lblClosed.Name = "lblClosed";
            this.lblClosed.Size = new System.Drawing.Size(79, 25);
            this.lblClosed.TabIndex = 7;
            this.lblClosed.Text = "Closed";
            // 
            // lblTrash
            // 
            this.lblTrash.AutoSize = true;
            this.lblTrash.Location = new System.Drawing.Point(23, 409);
            this.lblTrash.Name = "lblTrash";
            this.lblTrash.Size = new System.Drawing.Size(127, 13);
            this.lblTrash.TabIndex = 8;
            this.lblTrash.Text = "Drop task here to remove";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(565, 451);
            this.Controls.Add(this.lblTrash);
            this.Controls.Add(this.lblClosed);
            this.Controls.Add(this.lblResolved);
            this.Controls.Add(this.lblBacklog);
            this.Controls.Add(this.lbTrash);
            this.Controls.Add(this.btnAddTask);
            this.Controls.Add(this.lbClosed);
            this.Controls.Add(this.lbResolved);
            this.Controls.Add(this.lbBacklog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "TODO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbBacklog;
        private System.Windows.Forms.ListBox lbResolved;
        private System.Windows.Forms.ListBox lbClosed;
        private System.Windows.Forms.Button btnAddTask;
        private System.Windows.Forms.ListBox lbTrash;
        private System.Windows.Forms.Label lblBacklog;
        private System.Windows.Forms.Label lblResolved;
        private System.Windows.Forms.Label lblClosed;
        private System.Windows.Forms.Label lblTrash;
    }
}

