﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDoApp_v1.View;

namespace ToDoApp_v1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitialData();
        }

        // Drag function
        private void lb_MouseDown(object sender, MouseEventArgs e)
        {
            ListBox lb = sender as ListBox;
            if (lb.Items.Count == 0)
                return;
            int index = lb.IndexFromPoint(e.X, e.Y);
            if (index < 0)
                return;   
            DragDropEffects dd = DoDragDrop(lb.Items[index], DragDropEffects.All);
            if (dd == DragDropEffects.All)
            {
                lb.Items.RemoveAt(index);
            }
        }
        
        // DragOver function
        private void lb_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        // DragDrop function
        private void lb_DragDrop(object sender, DragEventArgs e)
        {
            ListBox lb = sender as ListBox;
            Task task = (Task)e.Data.GetData(typeof(Task));
            lb.Items.Add(task);
        }

        // Drop to remove function
        private void lbTrash_DragDrop(object sender, DragEventArgs e)
        {
            Task task = (Task)e.Data.GetData(typeof(Task));
            string message = "Delete task: " + task.Title + " success";
            MessageBox.Show(message);
            return;
        }

        //Add New Task
        private void btnAddTask_Click(object sender, EventArgs e)
        {
            // 1. Open child form to input data
            // 2. Child form return a task
            // 3. Check type of task then add to Listbox
            CreateTaskForm form = new CreateTaskForm();
            form.ShowDialog();
            Task task = form.SendData();
            if (task.Title != "")
            {
                switch (task.Type)
                {
                    case 0:
                        lbBacklog.Items.Add(task);
                        break;
                    case 1:
                        lbResolved.Items.Add(task);
                        break;
                    case 2:
                        lbClosed.Items.Add(task);
                        break;
                    default:
                        break;
                }
                MessageBox.Show("Add new task success");
            }
        }
        public void InitialData()
        {
            lbBacklog.Items.Add(new Task("Task2", "Task2", DateTime.Now, DateTime.Now, DateTime.Now, 0));
            lbBacklog.Items.Add(new Task("Task3", "Task3", DateTime.Now, DateTime.Now, DateTime.Now, 0));
            lbResolved.Items.Add(new Task("Task22", "Task2", DateTime.Now, DateTime.Now, DateTime.Now, 1));
            lbResolved.Items.Add(new Task("Task32", "Task3", DateTime.Now, DateTime.Now, DateTime.Now, 1));
            lbClosed.Items.Add(new Task("Task13", "Task1", DateTime.Now, DateTime.Now, DateTime.Now, 2));
            lbClosed.Items.Add(new Task("Task23", "Task2", DateTime.Now, DateTime.Now, DateTime.Now, 2));
            lbClosed.Items.Add(new Task("Task33", "Task3", DateTime.Now, DateTime.Now, DateTime.Now, 2));
        }
    }
}
