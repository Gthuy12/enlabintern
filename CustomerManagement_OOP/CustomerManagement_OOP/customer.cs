﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement_OOP
{
    public class customer
    {
        //Customer variable
        private int ID;
        private string Name;
        private string Address;
        private List<contact> Contacts;

        //Constructor
        public customer(int id, string name, string address, List<contact> ct) {
            this.ID = id;
            this.Name = name;
            this.Address = address;
            this.Contacts = ct;
        }
        public List<contact> contacts { get => Contacts; set => Contacts = value; }
        public int id { get => ID; set => ID = value; }
        public string name { get => Name; set => Name = value; }
        public string address { get => Address; set => Address = value; }

        public void Show()
        {
            Console.WriteLine("========= CUSTOMER INFO =========");
            Console.WriteLine("User {0}: {1}, {2}", this.id, this.name, this.address);
        }
    }
}
