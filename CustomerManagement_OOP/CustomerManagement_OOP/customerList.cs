﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement_OOP
{
    public class customerList
    {
        //Properties
        private List<customer> lists;
        public List<customer> Lists { get => lists; set => lists = value; }
        //Constructor
        public customerList()
        {
            // 1. Define new list
            // 2. Read file to create list
            string line;
            this.lists = new List<customer>();
            System.IO.StreamReader file = new System.IO.StreamReader(
                @"C:\Users\thuy.tran\source\repos\CustomerManagement_OOP\CustomerManagement_OOP\CustomerList.txt");
            while ((line = file.ReadLine()) != null)
            {
                string[] words = line.Split(';');
                lists.Add(new customer(Convert.ToInt32(words[0]), words[1], words[2], new List<contact>()));
            }
            file.Close();
        }

        //METHODS
        #region Main functions
        //View List Customers
        public void Show()
        {
            Console.Clear();
            Console.WriteLine("================== USER LIST ==================");
            foreach (var cus in this.lists)
            {
                Console.WriteLine("---------------------------------");
                Console.WriteLine("User {0}: {1}, {2}", cus.id, cus.name, cus.address);
                foreach (var add in cus.contacts)
                {
                    Console.WriteLine("     Address {0}: {1}, {2}", add.ContactName, add.Email, add.Phone);
                }
            }
            Console.Write("\r\nPress Enter to return to Main Menu");
            Console.ReadLine();
        }
        //Get One Customer
        public customer GetUser(int ID)
        {
            // 1. Get one user by ID
            customer cus = this.lists.Find(item => item.id == ID);
            return cus;
        }
        //Add Customer to List
        public void CreateUser()
        {
            // 1. Input ID and check exist
            // 2. Input another informations
            // 3. Add to list and return Main menu
            Console.Clear();
            Console.WriteLine("Enter identify:");
            int id = Convert.ToInt32(Console.ReadLine());
            while (CheckExist(id))
            {
                Console.WriteLine("This ID already exist, please input again !!");
                Console.WriteLine("Enter identify:");
                id = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("Enter username:");
            string userName = Console.ReadLine();
            Console.WriteLine("Enter address:");
            string address = Console.ReadLine();
            Console.WriteLine("Enter number of contact:");
            int count = Convert.ToInt32(Console.ReadLine());
            this.lists.Add(new customer(id, userName, address, CreateListContacts(count)));
            Console.Write("\r\nCreate new user success. Press Enter to return to Main Menu");
            Console.ReadLine();
        }
        //Edit user
        public void EditUser()
        {
            // 1. Input ID and get customer by ID
            // 2. Check customer exist -> null -> Ask to input ID again
            //                         -> not null -> Show customer Info and Menu 
            Console.Clear();
            Console.WriteLine("Enter user ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            customer cus = GetUser(id);
            if (cus == null)
            {
                Console.Write("\r\nCan not find any user !! Do you want to continue ? Y/N");
                string check = Console.ReadLine();
                if (check == "Y" || check == "y")
                {
                    EditUser();
                }
                return;
            }
            else
            {
                GetUser(id).Show();
                bool checkMenu = true;
                while (checkMenu)
                {
                    checkMenu = EditMenu(id);
                }
            }
        }
        //Delete user
        public void DeleteUser()
        {
            // 1. Input ID and get customer by ID
            // 2. Check customer exist -> null -> Main Menu
            //                         -> not null -> Confirm delete -> Remove customer from list
            Console.Clear();
            Console.WriteLine("Enter user ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            customer cus = GetUser(id);
            if (cus == null)
            {
                Console.Write("\r\nCan not find any user !!. Press Enter to return to Main Menu");
                Console.ReadLine();
            }
            else
            {
                GetUser(id).Show();
                Console.WriteLine("Do you want to delete user? Y/N");
                string check = Console.ReadLine();
                while (check == "Y" || check == "y")
                {
                    this.lists.RemoveAt(this.lists.FindIndex(item => item.id == id));
                    Console.WriteLine("Delete user success !!");
                }
                Console.Write("\r\nPress Enter to return to Main Menu");
                Console.ReadLine();
            }
        }
        //Save list to file
        public void SaveList()
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\Users\thuy.tran\source\repos\CustomerManagement_OOP\CustomerManagement_OOP\CustomerList.txt"))
            {
                foreach (customer cus in this.lists)
                {
                    string line = cus.id.ToString() + ";" + cus.name + ";" + cus.address + ";";
                    foreach (contact ct in cus.contacts)
                    {
                        line += ct.ContactName + "/" + ct.Email + "/" + ct.Phone + ";";
                    }
                    file.WriteLine(line);
                }
            }
            Console.Write("\r\nSave file success. Press Enter to return to Main Menu");
            Console.ReadLine();
        }
        #endregion

        #region Helper functions for edit user
        //List options for edit user
        public bool EditMenu(int id)
        {
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1/Edit Name and Address");
            Console.WriteLine("2/Edit Contact");
            Console.WriteLine("3/Quit");
            switch (Console.ReadLine())
            {
                case "1":
                    EditMainInfo(id);
                    return true;
                case "2":
                    EditContactInfo(id);
                    return true;
                case "3":
                    return false;
                default:
                    return true;
            }
        }
        //Edit user within Main Infomation
        public void EditMainInfo(int id)
        {
            // 1. Save contact info to a templist
            // 2. Input new main info
            // 3. Change main info
            List<contact> tempList = this.lists[this.lists.FindIndex(item => item.id == id)].contacts;
            Console.WriteLine("Enter username:");
            string userName = Console.ReadLine();
            Console.WriteLine("Enter address:");
            string address = Console.ReadLine();
            this.lists[this.lists.FindIndex(item => item.id == id)] = new customer(id, userName, address, tempList);
            Console.Write("\r\nUpdate success. Press Enter to return to Main Menu");
            Console.ReadLine();
        }
        //Edit user's Contact Infomation
        public void EditContactInfo(int id)
        {
            // 1. Save main info to a temp variable
            // 2. Input new list contacts
            // 3. Change list contacts
            customer tempCus = this.lists[this.lists.FindIndex(item => item.id == id)];
            string userName = tempCus.name;
            string address = tempCus.address;
            List<contact> list = new List<contact>();
            Console.WriteLine("Enter number of contacts:");
            int count = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("Enter contact name:");
                string ctName = Console.ReadLine();
                Console.WriteLine("Enter email contact:");
                string ctEmail = Console.ReadLine();
                Console.WriteLine("Enter phone contact:");
                string ctPhone = Console.ReadLine();
                list.Add(new contact(ctName, ctEmail, ctPhone));
            }
            this.lists[this.lists.FindIndex(item => item.id == id)] = new customer(id, userName, address, list);
            Console.Write("\r\nUpdate success. Press Enter to return to Main Menu");
            Console.ReadLine();
        }
        #endregion

        #region Helper functions to work with Contact Information (Edit and Create user)
        //Create new list contact
        public List<contact> CreateListContacts(int count)
        {
            List<contact> listContacts = new List<contact>();
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("     ----- Contact {0} -----", i + 1);
                Console.WriteLine("Enter contact name:");
                string ctName = Console.ReadLine();
                Console.WriteLine("Enter email contact:");
                string ctEmail = Console.ReadLine();
                Console.WriteLine("Enter phone contact:");
                string ctPhone = Console.ReadLine();
                listContacts.Add(new contact(ctName, ctEmail, ctPhone));
            }
            return listContacts;
        }
        //Create new list contact read file
        public List<contact> CreateListContactsFile(string[] listItem)
        {
            List<contact> listContacts = new List<contact>();
            for (int i = 3; i < listItem.Length; i++)
            {
                string[] words = listItem[i].Split('/');
                listContacts.Add(new contact(words[0], words[1], words[2]));
            }
            return listContacts;
        }
        #endregion

        #region Helper fucntions for validate input
        //Check user already exist
        public bool CheckExist(int id)
        {
            customer cus = this.lists.Find(item => item.id == id);
            if (cus == null)
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
