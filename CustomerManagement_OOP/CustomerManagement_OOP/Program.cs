﻿using System;
using System.Collections.Generic;

namespace CustomerManagement_OOP
{
    class Program
    {
        static readonly customerList _lists = new customerList();
        static void Main(string[] args)
        {
            bool checkMenu = true;
            while(checkMenu)
            {
                checkMenu = MainMenu();
            }
        }
        //Main menu
        static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1/Show list user");
            Console.WriteLine("2/Create new user");
            Console.WriteLine("3/Edit user");
            Console.WriteLine("4/Delete user");
            Console.WriteLine("5/Save File");
            Console.WriteLine("6/Quit");
            switch(Console.ReadLine())
            {
                case "1":
                    _lists.Show();
                    return true;
                case "2":
                    _lists.CreateUser();
                    return true;
                case "3":
                    _lists.EditUser();
                    return true;
                case "4":
                    _lists.DeleteUser();
                    return true;
                case "5":
                    _lists.SaveList();
                    return true;
                case "6":
                    return false;
                default:
                    return true;
            }
        }
    }
}
