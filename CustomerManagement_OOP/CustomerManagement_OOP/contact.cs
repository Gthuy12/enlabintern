﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement_OOP
{
    public class contact
    {
        //Properties
        private string contactName;
        private string email;
        private string phone;

        //Constructor
        public contact(string ctName, string ctEmail, string ctPhone)
        {
            this.contactName = ctName;
            this.email = ctEmail;
            this.phone = ctPhone;
        }
        public string ContactName { get => contactName; set => contactName = value; }
        public string Email { get => email; set => email = value; }
        public string Phone { get => phone; set => phone = value; }

        //Methods
        public void Show()
        {
            Console.WriteLine("========= CONTACT INFO =========");
            Console.WriteLine("Contact {0}: {1}, {2}", this.contactName, this.email, this.phone);
        }
    }
}
