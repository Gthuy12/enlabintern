﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ToDoApp_V2.DTO;
using System.Threading.Tasks;
using Task = ToDoApp_V2.DTO.Task;

namespace ToDoApp_V2.DAL
{
    class DAL_Task
    {
        private static DAL_Task _Instance;

        public static DAL_Task Instance { 
            get
            {
                if (_Instance == null)
                {
                    _Instance = new DAL_Task();
                }
                return _Instance;
            } 
            private set => _Instance = value; 
        }
        public DataTable DAL_GetTasks ()
        {
            try
            { 
                string query = "select * from Task";
                return DataProvider.Instance.getRecord(query);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }
        public DataTable DAL_GetTasks (string lb)
        {
            try
            {               
                string query = "select * from Task inner join ListTask on Task.ID_ListTask = ListTask.ID where CHARINDEX(ListTask.name,'"+ lb.ToString() +"') > 0";
                return DataProvider.Instance.getRecord(query);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }
        public Task GetTaskByRow(DataRow r)
        {
            return new Task
            {
                ID = Int32.Parse(r["ID"].ToString()),
                Title = r["title"].ToString(),
                Description = r["description"].ToString(),
                CreatedAt = Convert.ToDateTime(r["createdAt"].ToString()),
                FinishedAt = Convert.ToDateTime(r["finishedAt"].ToString()),
                UpdatedAt = Convert.ToDateTime(r["updatedAt"].ToString()),
                IDlist = Int32.Parse(r["ID_ListTask"].ToString())
            };
        }
        public bool UpdateTaskList(int taskId, string listName)
        {
            try
            {
                string query = "update Task set ID_ListTask = (select ID from ListTask where CHARINDEX(ListTask.name,'"+ listName +"') > 0) where ID = " + taskId;
                DataProvider.Instance.getRecord(query);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddTask(string title, string des, DateTime create, DateTime update, DateTime finish, string idList)
        {
            try
            {
                //Call data provider
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
