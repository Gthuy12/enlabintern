﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoApp_V2.DAL
{
    class DataProvider
    {
        private static DataProvider _Instance;
        private string strcnn;

        public static DataProvider Instance { 
            get {
                if(_Instance == null )
                {
                    _Instance = new DataProvider();
                }
                return _Instance;
            } 
            private set => _Instance = value; 
        }

        private DataProvider()
        {
            strcnn = @"Data Source= ThuyTran-PC; Initial Catalog =ToDoList; Integrated Security=True";
        }

        public DataTable getRecord(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                using(SqlConnection cnn = new SqlConnection(strcnn))
                {
                    SqlDataAdapter da = new SqlDataAdapter(query, cnn);
                    cnn.Open();
                    da.Fill(dt);
                    cnn.Close();
                }
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
