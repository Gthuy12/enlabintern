﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoApp_V2.DTO
{
    public class Task
    {
        private int _ID;
        private string _Title;
        private string _Description;
        private DateTime _CreatedAt;
        private DateTime _UpdatedAt;
        private DateTime _finishedAt;
        private int _IDlist;

        public DateTime CreatedAt { get => _CreatedAt; set => _CreatedAt = value; }
        public string Title { get => _Title; set => _Title = value; }
        public string Description { get => _Description; set => _Description = value; }
        public DateTime UpdatedAt { get => _UpdatedAt; set => _UpdatedAt = value; }
        public DateTime FinishedAt { get => _finishedAt; set => _finishedAt = value; }
        public int IDlist { get => _IDlist; set => _IDlist = value; }
        public int ID { get => _ID; set => _ID = value; }
    }
}
