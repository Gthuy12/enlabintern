﻿
namespace ToDoApp_V2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbBacklog = new System.Windows.Forms.ListBox();
            this.lbResolved = new System.Windows.Forms.ListBox();
            this.lbClosed = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbBacklog
            // 
            this.lbBacklog.AllowDrop = true;
            this.lbBacklog.DisplayMember = "title";
            this.lbBacklog.FormattingEnabled = true;
            this.lbBacklog.Location = new System.Drawing.Point(22, 21);
            this.lbBacklog.Name = "lbBacklog";
            this.lbBacklog.Size = new System.Drawing.Size(140, 342);
            this.lbBacklog.TabIndex = 0;
            this.lbBacklog.DragDrop += new System.Windows.Forms.DragEventHandler(this.lb_DragDrop);
            this.lbBacklog.DragOver += new System.Windows.Forms.DragEventHandler(this.lb_DragOver);
            this.lbBacklog.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_MouseDown);
            // 
            // lbResolved
            // 
            this.lbResolved.AllowDrop = true;
            this.lbResolved.DisplayMember = "title";
            this.lbResolved.FormattingEnabled = true;
            this.lbResolved.Location = new System.Drawing.Point(199, 21);
            this.lbResolved.Name = "lbResolved";
            this.lbResolved.Size = new System.Drawing.Size(140, 342);
            this.lbResolved.TabIndex = 1;
            this.lbResolved.DragDrop += new System.Windows.Forms.DragEventHandler(this.lb_DragDrop);
            this.lbResolved.DragOver += new System.Windows.Forms.DragEventHandler(this.lb_DragOver);
            this.lbResolved.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_MouseDown);
            // 
            // lbClosed
            // 
            this.lbClosed.AllowDrop = true;
            this.lbClosed.DisplayMember = "title";
            this.lbClosed.FormattingEnabled = true;
            this.lbClosed.Location = new System.Drawing.Point(376, 21);
            this.lbClosed.Name = "lbClosed";
            this.lbClosed.Size = new System.Drawing.Size(140, 342);
            this.lbClosed.TabIndex = 2;
            this.lbClosed.DragDrop += new System.Windows.Forms.DragEventHandler(this.lb_DragDrop);
            this.lbClosed.DragOver += new System.Windows.Forms.DragEventHandler(this.lb_DragOver);
            this.lbClosed.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_MouseDown);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(441, 377);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "ADD TASK";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 412);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbClosed);
            this.Controls.Add(this.lbResolved);
            this.Controls.Add(this.lbBacklog);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbBacklog;
        private System.Windows.Forms.ListBox lbResolved;
        private System.Windows.Forms.ListBox lbClosed;
        private System.Windows.Forms.Button btnAdd;
    }
}

