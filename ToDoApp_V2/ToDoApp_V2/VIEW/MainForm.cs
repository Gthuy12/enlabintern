﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDoApp_V2.BLL;
using ToDoApp_V2.DAL;
using Task = ToDoApp_V2.DTO.Task;

namespace ToDoApp_V2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadSingleListBox(lbBacklog);
            LoadSingleListBox(lbResolved);
            LoadSingleListBox(lbClosed);
        }

        public void LoadSingleListBox(ListBox lb)
        {
            lb.Items.Clear();
            var tasks = BLL_Task.Instance.BLL_GetTasks(lb.Name.ToString());
            foreach (var task in from DataRow r in tasks.Rows
                                       let task = DAL_Task.Instance.GetTaskByRow(r)
                                       let id = task.IDlist
                                       select task)
                lb.Items.Add(task);
        }

        private void lb_MouseDown(object sender, MouseEventArgs e)
        {
            ListBox lb = sender as ListBox;
            if (lb.Items.Count == 0)
                return;
            int index = lb.IndexFromPoint(e.X, e.Y);
            if (index < 0)
                return;
            DragDropEffects dd = DoDragDrop(lb.Items[index], DragDropEffects.All);
            if (dd == DragDropEffects.All)
            {
                LoadSingleListBox(lb);
            }
        }

        private void lb_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void lb_DragDrop(object sender, DragEventArgs e)
        {
            ListBox lb = sender as ListBox;
            if(e.Data.GetDataPresent(typeof(Task))) 
            {
                Task task = (Task)e.Data.GetData(typeof(Task));
                try
                {
                    if(BLL_Task.Instance.BLL_UpdateTaskList(task.ID,lb.Name.ToString()))
                    {
                        LoadSingleListBox(lb);
                    } else
                    {
                        MessageBox.Show("Drag drop failed");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Drag drop failed");
                }
            }
        }
    }
}
