﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task = ToDoApp_V2.DTO.Task;

namespace ToDoApp_V2.VIEW
{
    public partial class AddTaskForm : Form
    {
        public AddTaskForm()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validate() == 1)
            {
                MessageBox.Show("Title and Description must be not null");
            }
            else if (validate() == 2)
            {
                MessageBox.Show("Duedate must be later");
            }
            else
            {
                AddData();
                this.Close();
            }
        }

        //Send Data to create new Task
        // 1. Input data
        // 2. Create new task and return
        public bool AddData()
        {
            string title = tbTitle.Text.ToString();
            DateTime finish = dtpFinish.Value;
            string description = tbDescription.Text.ToString();
            //Task task = new Task(title, description, DateTime.Now, DateTime.Now, finish, cbType.SelectedIndex);
            try
            {
                if()
                return true;
            }
            catch (Exception)
            {
                return false;
            }
           
        }
        public int validate()
        {
            if (tbTitle.Text.ToString() == "" || tbDescription.Text.ToString() == "")
            {
                return 1;
            }
            if (DateTime.Compare(dtpFinish.Value, DateTime.Now) < 0)
            {
                return 2;
            }
            return 0;
        }
    }
}
