﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ToDoApp_V2.DAL;
using System.Threading.Tasks;

namespace ToDoApp_V2.BLL
{
    class BLL_Task
    {
        private static BLL_Task _Instance;

        internal static BLL_Task Instance 
        { 
            get
            {
                if (_Instance == null)
                {
                    _Instance = new BLL_Task();
                }
                return _Instance;
            }
            private set => _Instance = value; 
        }

        public DataTable BLL_GetTasks()
        {
            return DAL_Task.Instance.DAL_GetTasks();
        }
        public DataTable BLL_GetTasks(string lb)
        {
            return DAL_Task.Instance.DAL_GetTasks(lb);
        }

        public bool BLL_UpdateTaskList(int i, string s)
        {
            return DAL_Task.Instance.UpdateTaskList(i, s);
        }
    }
}
