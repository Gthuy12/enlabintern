﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoApplication.Module.Interfaces
{
    public interface IUnitOfWork
    {
        void SaveChanges();
    }
}
