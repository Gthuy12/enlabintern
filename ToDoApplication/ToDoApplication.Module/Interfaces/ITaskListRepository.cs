﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoApplication.Module.Entities;

namespace ToDoApplication.Module.Interfaces
{
    public interface ITaskListRepository : IRepository<ListTask>
    {

    }
}
