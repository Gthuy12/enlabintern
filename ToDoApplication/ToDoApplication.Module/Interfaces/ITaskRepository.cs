﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoApplication.Module.Entities;

namespace ToDoApplication.Module.Interfaces
{
    public interface ITaskRepository : IRepository<Task>
    {
        public List<Task> GetTasks(int id);
    }
}
