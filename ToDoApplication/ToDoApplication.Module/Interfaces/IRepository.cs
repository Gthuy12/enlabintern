﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoApplication.Module.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
