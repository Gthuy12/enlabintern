﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoApplication.Module.Entities;
using Microsoft.EntityFrameworkCore;

namespace ToDoApplication.Infrastructure
{
    public class AppDbContext : DbContext
    {
        public DbSet<Task> Task { get; set; }
        public DbSet<ListTask> Tasks { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
