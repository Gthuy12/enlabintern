﻿using System;
using ToDoApplication.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.Text;

namespace ToDoApplication.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbFactory _dbFactory;

        public UnitOfWork(DbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public void SaveChanges()
        {
            _dbFactory.DbContext.SaveChanges();
        }
    }
}
