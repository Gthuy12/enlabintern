﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorWinFormOOP
{
    public partial class Calculator : Form
    {
        public Calculator()
        {
            InitializeComponent();
        }

        #region Input buttons
        private void btn_Click(object sender, EventArgs e)
        {
            string s = (sender as Button).Text;
            if (tbResult.Text == "0" || tbResult.Text == null)
            {
                tbResult.Text = s;
            }
            else
            {
                tbResult.Text += s;
            }
        }
        // Delete last character
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (tbResult.Text.ToString().Length > 0)
            {
                string calculate = tbResult.Text.ToString();
                string calculateTemp = calculate.Remove(calculate.Length - 1, 1);
                tbResult.Text = calculateTemp;
            }
        }
        // Delete all characters
        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            tbResult.Text = null;
        }
        #endregion

        // Get result
        private void btnEqual_Click(object sender, EventArgs e)
        {
            // 1. Convert String in textbox to Postfix
            // 2. Evaluate Postfix
            // 3. Show result in textbox
            string show = PostFix.InfixToPostfix(tbResult.Text.ToString());
            double result = PostFix.EvaluatePostfix(show);
            tbResult.Text = result.ToString();
        }

        //Check Syntax (use for operator buttons)
        private bool CheckSyntax()
        {
            // 1. Get last character of calculation
            // 2. Check if it's a operator
            string check = tbResult.Text.ToString();
            string lastChar = check.Substring(check.Length - 1);
            if (lastChar == "/" || lastChar == "*" || lastChar == "+" || lastChar == "-" || lastChar == ".")
            {
                return false;
            }
            return true;
        }
    }
}
