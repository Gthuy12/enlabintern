﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CalculatorWinFormOOP
{
    class PostFix
    {
        // Convert Infix to Postfix
        public static string InfixToPostfix(string infix)
        {
            // 1. Create Stack and Output
            // 2. Split Infix by ' ' and save to str
            // 3. Check each element of str
            //          -> operand -> save to output
            //          -> operator -> If the first operator in stack have more priority or equal with current operator, save it to output
            //                      -> Add current operator to stack
            FormatExpression(ref infix);
            IEnumerable<string> str = infix.Split(' ');
            Stack<string> stack = new Stack<string>();
            StringBuilder postfix = new StringBuilder();
            foreach (string s in str)
            {
                if (IsOperator(s))
                {
                    while (stack.Count > 0 && GetPriority(s) <= GetPriority(stack.Peek()))
                        postfix.Append(stack.Pop()).Append(" ");
                    stack.Push(s);
                } else
                {
                    postfix.Append(s);
                    postfix.Append(" ");
                }
            }
            while (stack.Count > 0)
                postfix.Append(stack.Pop()).Append(" ");
            return postfix.ToString();
        }
        
        // Evaluate Postfix
        private static double EvaluatePostfix(IEnumerable<string> tokens)
        {
            // 1. read each elements in tokens
            // 2. If operand -> push to stack
            //    If operator -> pop 2 operands on top and calculate -> push result to stack
            // 3. Finally, stack have only 1 element, it is our result
            Stack<double> stack = new Stack<double>();
            foreach (string s in tokens)
            {
                if (IsOperator(s))
                {
                    double x = stack.Pop();
                    double y = stack.Pop();
                        switch (s)
                        {
                            case "+": y += x; break;
                            case "-": y -= x; break;
                            case "*": y *= x; break;
                            case "/": y /= x; break;        
                            default:
                                throw new Exception("Invalid operator");
                        }
                        stack.Push(y);   
                }
                else
                {
                    stack.Push(double.Parse(s));
                }
            }
            return stack.Pop();
        }

        #region Helper functions
        // Check priority 
        private static int GetPriority(string op)
        {
            if (op == "*" || op == "/")
                return 2;
            if (op == "+" || op == "-")
                return 1;
            return 0;
        }

        // Format Input
        private static void FormatExpression(ref string expression)
        {
            // 1. Remove all space
            // 2. Add space before and after operator
            // 3. Remove double space to space
            expression = expression.Replace(" ", "");
            expression = Regex.Replace(expression, @"\+|\-|\*|\/", delegate (Match match)
            {
                return " " + match.Value + " ";
            });
            expression = expression.Replace("  ", " ");
            expression = expression.Trim();
        }

        // Check character is operator
        private static bool IsOperator(string str)
        {
            return Regex.Match(str, @"\+|\-|\*|\/").Success;
        }

        //Trim and Split Postfix string
        public static double EvaluatePostfix(string postfix)
        {
            return EvaluatePostfix(postfix.Trim().Split(' '));
        }
        #endregion 
    }
}
